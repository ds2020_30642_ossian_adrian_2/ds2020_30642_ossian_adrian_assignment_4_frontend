import React from 'react';
import SockJS from 'sockjs-client'
import Stomp from 'stomp-websocket'


var stompClient = null;

class SampleComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            clientConnected: false,
            att: 0,
            messages: [],
            messagez: ""
        };
        this.connectCallback = this.connectCallback.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.onMessageReceive = this.onMessageReceive.bind(this);

    }


    componentDidMount() {

        //TODO: change this to backend url link
        var sock = new SockJS('http://localhost:8080/gs-guide-websocket');



        stompClient = Stomp.over(sock);
        stompClient.connect({}, this.connectCallback);
    }

    connectCallback(frame) {

        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/greetings', this.onMessageReceive);
    }

    onMessageReceive (greeting) {
        let receivedMessage = JSON.parse(greeting.body).content;
        this.setState(
            { messages: [...this.state.messages, receivedMessage] }
        )
        console.log(this.state.messages);
    }

    greeting(greeting) {
        console.log("plm");
        this.onMessageReceive(greeting);
        //this.showGreeting(JSON.parse(greeting.body).content);

        // let newAnomaly = JSON.parse(greeting.body).content
        //
        // this.setState(
        //     { groceries: [...this.state.groceries, newGrocery] }

//this.state.messages.concat(JSON.parse(greeting.body).content)
        // this.onMessageReceive((greeting.body).content);

        // this.setState(
        //     { messages: [...this.state.messages, receivedMessage] }
        // )


        console.log(JSON.parse(greeting.body).content);
    }


    sendMessage(){

    }


    render() {
        return (
            <div>
                {/*<TalkBox topic="react-websocket-template"*/}
                {/*          messages={ this.state.messages }*/}
                {/*         connected={ this.state.clientConnected }/>*/}
                {/*<TalkBox topic="react-websocket-template" currentUserId="ping" currentUser="Pinger"*/}
                {/*         messages={ this.state.messages } onSendMessage={ this.sendMessage } />*/}

                <React.Fragment>
                    <ul className="list-group">
                        {this.state.messages.map(message => (
                            <li className="list-group-item list-group-item-primary">
                                {message}
                            </li>
                        ))}
                    </ul>
                </React.Fragment>

            </div>
        );
    }
}

export default SampleComponent;