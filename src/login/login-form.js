import React from 'react';
import Form from 'react-bootstrap/Form';
import {Button, Col} from 'reactstrap';
import * as API_USERS from "../User/api/user-api";
import withRouter from "react-router/withRouter";

class FormPage extends React.Component {


    constructor(props) {
        super(props);

        this.checkUser = this.checkUser.bind(this);
        this.handleChangeInputUsername = this.handleChangeInputUsername.bind(this);
        this.handleChangeInputPassword = this.handleChangeInputPassword.bind(this);

        this.state = {


            username: "",
            password: "",
            errorStatus: 0,
            error: null
        };
    }

    loginUser(user) {
        return API_USERS.logInUser(user.username, user.password , (result, status, err) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("User exists");
                console.log(result);


                if(result.type === "caregiver"){
                    console.log("Welcome, caregiver!");
                    this.props.history.push('/patient/caregiver');
                    //window.location.href="/patient/caregiver"
                }
                else if(result.type === "doctor"){
                    console.log("Welcome, doctor!");
                    this.props.history.push('/doctor/home');
                    //window.location.href="/doctor/home"
                }
                else if(result.type === "patient"){
                    console.log("Welcome, patient!");
                    this.props.history.push('/patient/medication');
                    //window.location.href="/patient/medication"
                }



            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
                console.log("User does not exist");
            }

        });
    }

    checkUser() {

        let user = {
            username: this.state.username,
            password: this.state.password
        };

        console.log(user);
        this.loginUser(user);
    }

    handleChangeInputUsername(event) {
        this.setState({username: event.target.value});
    }

    handleChangeInputPassword(event) {
        this.setState({password: event.target.value});
    }

    render()
    {
        return (

            <div>

                <div className="col-sm-10 offset-sm-1 text-center">
                <Form>
                    <Form.Group controlId="username">
                        <Form.Label>Username</Form.Label>
                        <div
                            className="col-10 offset-1 col-lg-8 offset-lg-2 div-wrapper d-flex justify-content-center align-items-center">

                            <div className="div-to-align">
                                <Form.Control type="text" placeholder="Enter username" value={this.state.username} onChange={this.handleChangeInputUsername}/>
                            </div>

                        </div>

                    </Form.Group>

                    <Form.Group controlId="formPassword">
                        <Form.Label>Password</Form.Label>
                        <div
                            className="col-10 offset-1 col-lg-8 offset-lg-2 div-wrapper d-flex justify-content-center align-items-center">

                            <div className="div-to-align">
                                <Form.Control type="password" placeholder="Password" value={this.state.password} onChange={this.handleChangeInputPassword}/>
                            </div>

                        </div>

                    </Form.Group>
                    <Button color="primary" onClick={this.checkUser}>
                        Log in
                    </Button>



                    <Col sm={{size: '8', offset: 2}}>
                        {this.state.errorStatus > 0 &&
                        < h4 > Bad username or password </h4>
                        }
                    </Col>


                </Form>
                </div>



            </div>


        );
    }
    ;
}

export default withRouter(FormPage);
