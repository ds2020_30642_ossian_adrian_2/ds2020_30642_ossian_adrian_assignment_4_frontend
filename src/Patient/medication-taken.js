import React from 'react';
import Clock from 'react-live-clock';
import ReactFitText from 'react-fittext';
import * as API_USERS from "./api/medication-taken-api"
import Table from "../commons/tables/table";
import {
    Button
} from 'reactstrap';
import {Col} from "react-bootstrap";


const columns = [
    {
        Header: 'Medication name',
        accessor: 'name',
    },
    {
        Header: 'Date',
        accessor: 'date',
    }
];

const filters = [
    {
        accessor: ' ',
    }
];

class MedicationTaken extends React.Component {

    constructor(props) {
        super(props);

        this.fetchMedication = this.fetchMedication.bind(this);
        this.takeOneMedication = this.takeOneMedication.bind(this);
        this.buttonClickTakeMedication = this.buttonClickTakeMedication.bind(this);
        this.buttonClickReset = this.buttonClickReset.bind(this);
        this.wasMedicationTakenInThePredefinedTime = this.wasMedicationTakenInThePredefinedTime.bind(this);


        this.state = {
            medicationData: [],
            selectedTeam: "",
            isLoaded: false,
            medicationTakenMessage: true,
            medicationCorrect: false
        };
    }



    componentDidMount() {

        this.fetchMedication();


    }

    fetchMedication(){

        return API_USERS.getMedication((result, status, err) => {
            if (result !== null && status === 200) {

                this.setState({
                    medicationData: result,
                    isLoaded: true,
                    medicationTakenMessage:false,
                    medicationCorrect:false
                });
                console.log(result);

            }
        });

    }



    takeOneMedication(){

        return API_USERS.takeOneMedication((result, status, err) => {
            if (result !== null && status === 200) {

                this.setState({
                    medicationData: result
                });
                console.log(result);

            }
        });

    }

    wasMedicationTakenInThePredefinedTime() {

        return API_USERS.wasMedicationTakenCorrectly((result, status, err) => {
            if (status === 200) {
                this.setState({
                    isLoaded: true,
                    medicationTakenMessage: true,
                    medicationCorrect: true

                });
            }

            else if(status === 410){

                this.setState({
                    isLoaded: true,
                    medicationTakenMessage: true,
                    medicationCorrect: false
                });

            }
        });


    }

    buttonClickTakeMedication() {

        this.setState({
            isLoaded: false
        });

        this.takeOneMedication();

        this.wasMedicationTakenInThePredefinedTime();

    }

    buttonClickReset() {

        this.setState({
            isLoaded: false
        });

        this.fetchMedication();


    }


    render() {
        return (
            <div>

                <ReactFitText  compressor={0.6}>
                    <h1>
                        <Clock
                            format={'HH:mm:ss'}
                            ticking={true}
                            timezone={'Europe/Bucharest'} />
                    </h1>
                </ReactFitText>

                <Button style={{ marginLeft: "auto" }} color="primary" onClick={this.buttonClickTakeMedication}>Take medication </Button>
                <Button style={{ marginLeft: "auto" }} color="primary" onClick={this.buttonClickReset}>Reset </Button>
                {this.state.medicationTakenMessage && this.state.medicationCorrect &&
                <div className="alert alert-success" role="alert">
                    Success! Medication was taken.
                </div>
                }

                {this.state.medicationTakenMessage &&  (! this.state.medicationCorrect) &&
                <div className="alert alert-danger" role="alert">
                    Danger! Medication wasn't taken correctly.
                </div>
                }

                {this.state.isLoaded && <Table
                    data={this.state.medicationData}
                    columns={columns}
                    search={filters}
                    pageSize={10}
                />}

            </div>
        );
    }
}

export default MedicationTaken;