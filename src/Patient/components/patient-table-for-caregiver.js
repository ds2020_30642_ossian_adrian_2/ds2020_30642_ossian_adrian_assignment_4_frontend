import React from "react";
import Table from "../../commons/tables/table";


const columns = [
    {
        Header: 'Name',
        accessor: 'name',
    },
    {
        Header: 'Address',
        accessor: 'adress',
    },
    {
        Header: 'Gender',
        accessor: 'gender',
    },
    {
        Header: 'Birthdate',
        accessor: 'birthdate',
    }
];

const filters = [
    {
        accessor: 'name',
    }
];

class PatientTableForCaregiver extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={filters}
                pageSize={5}
            />
        )
    }
}

export default PatientTableForCaregiver;