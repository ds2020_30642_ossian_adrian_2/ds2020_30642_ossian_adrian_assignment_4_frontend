import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    patient: '/medicationtaken'
};

function getMedication(callback) {
    let request = new Request(HOST.backend_api + endpoint.patient, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function takeOneMedication(callback) {
    let request = new Request(HOST.backend_api + endpoint.patient + "/takeone", {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function wasMedicationTakenCorrectly(callback) {
    let request = new Request(HOST.backend_api + endpoint.patient + "/wascorrect", {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    getMedication,
    takeOneMedication,
    wasMedicationTakenCorrectly

};
