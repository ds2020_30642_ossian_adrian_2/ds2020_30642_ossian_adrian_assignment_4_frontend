import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Row
} from 'reactstrap';


import * as API_USERS from "./api/patient-api"
import PatientMedicationTable from "./components/patient-medication-table";



class PatientUIContainer extends React.Component {

    constructor(props) {
        super(props);
        this.buttonClickPillbox = this.buttonClickPillbox.bind(this);
        this.state = {
            selected: false,
            selected_edit: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            error_log_in: 0,
            caregiverName: "caregiver1",
            name: "",
            birthdate: "",
            gender: "",
            address: "",
            isLoaded_user: false
        };
    }

    componentDidMount() {
        this.fetchUser();
    }

    fetchUser(){
        return API_USERS.getCurrent((result, status, err) => {
            if (result !== null && status === 200) {
                if(result.type === "patient"){
                    this.setState({
                        name: result.name,
                        birthdate: result.birthdate,
                        gender: result.gender,
                        address: result.adress,
                        isLoaded_user: true
                    });
                    this.fetchMedication(this.state.name);
                }
                else{
                    window.location.href="/user/login"
                    this.setState({error_log_in: 1});
                }
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    logOutUser() {
        return API_USERS.logOutUser( (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully logged out");
                window.location.href="/user/login"
            } else {
                console.log("Could not log out");
            }

        });
    }

    buttonClickPillbox(){
        window.location.href="/medicationtaken"
    }

    fetchMedication(patientName) {
        return API_USERS.getMedicationForPatient(patientName, (result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }


    render() {
        return (
            <div>
                <Row>
                    {this.state.error_log_in > 0 && <Col>
                        Sorry, you can't access this page.
                    </Col>}
                </Row>
                <Row>
                    <Col>
                        <CardHeader>
                            <strong> Patient medication plan </strong>
                        </CardHeader>
                    </Col>
                    <Col>
                    <div style={{ display: "flex" }}>
                        <Button style={{ marginLeft: "auto" }} color="primary" onClick={this.logOutUser}>Log out </Button>
                    </div>

                </Col>
                </Row>

                <Card>
                    <br/>
                    <Row>
                        <Col>
                            Hello, {this.state.name}
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            this is your birthday: {this.state.birthdate}
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            this is your gender: {this.state.gender}
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            and this is your address: {this.state.address}
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            Press to view pillbox manager:
                            <Button style={{ marginLeft: "auto" }} color="primary" onClick={this.buttonClickPillbox}>Pillbox manager </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col>
                            Below is your list of medication you're currently taking
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <PatientMedicationTable tableData = {this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>


            </div>
        )

    }
}


export default PatientUIContainer;
