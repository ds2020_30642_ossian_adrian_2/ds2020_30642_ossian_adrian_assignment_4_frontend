import React from 'react';
import SockJS from 'sockjs-client';
import Stomp from 'stomp-websocket';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Row
} from 'reactstrap';


import * as API_USERS from "./api/patient-api"
import PatientTableForCaregiver from "./components/patient-table-for-caregiver";



var stompClient = null;


class PatientsForCaregiverContainer extends React.Component {

    constructor(props) {
        super(props);
        this.handlePatientsForCaregiver = this.handlePatientsForCaregiver.bind(this);
        this.handleChangeInputCaregiverName = this.handleChangeInputCaregiverName.bind(this);
        this.connectCallback = this.connectCallback.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.onMessageReceive = this.onMessageReceive.bind(this);
        this.state = {
            selected: false,
            selected_edit: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            error_log_in: 0,
            caregiverName: "caregiver1",
            name: "",
            birthdate: "",
            gender: "",
            address: "",
            isLoaded_user: false,
            messages: []
        };
    }

    componentDidMount() {
        this.fetchUser();
        //TODO: change this to backend url link
        var sock = new SockJS('http://localhost:8080/gs-guide-websocket');
        //var sock = new SockJS('http://medicalservicesadrian2.herokuapp.com/gs-guide-websocket');

        stompClient = Stomp.over(sock);
        stompClient.connect({}, this.connectCallback);
    }

    connectCallback(frame) {

        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/greetings', this.onMessageReceive);
    }

    onMessageReceive (greeting) {
        let receivedMessage = JSON.parse(greeting.body).content;
        this.setState(
            { messages: [...this.state.messages, receivedMessage] }
        )
        console.log(this.state.messages);
    }


    fetchUser(){
        return API_USERS.getCurrent((result, status, err) => {
            if (result !== null && status === 200) {
                if(result.type === "caregiver"){
                    this.setState({
                        name: result.name,
                        birthdate: result.birthdate,
                        gender: result.gender,
                        address: result.adress,
                        isLoaded_user: true
                    });
                    this.fetchPatients(this.state.name);
                }
                else{
                    window.location.href="/user/login"
                    this.setState({error_log_in: 1});
                }
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    logOutUser() {
        return API_USERS.logOutUser( (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully logged out");
                window.location.href="/user/login"
            } else {
                console.log("Could not log out");
            }

        });
    }

    fetchPatients(caregiverName) {
        return API_USERS.getPatientsForCaregiver(caregiverName, (result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    searchPatientsForCaregiver(caregiver) {
        return API_USERS.getPatientsForCaregiver(caregiver, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully loaded patients");
            } else {
                console.log("Could not load patients");
            }

        });
    }

    handlePatientsForCaregiver() {

        console.log(this.state.caregiverName);
        this.searchPatientsForCaregiver(this.state.caregiverName);
        this.fetchPatients(this.state.caregiverName);
    }

    handleChangeInputCaregiverName(event) {
        this.setState({caregiverName: event.target.value});
    }



    render() {
        return (
            <div>
                <Row>
                    {this.state.error_log_in > 0 && <Col>
                        Sorry, you can't access this page.
                    </Col>}
                </Row>
                <Row>
                    <Col>
                        <CardHeader>
                            <strong> Patient Management </strong>
                        </CardHeader>
                    </Col>
                    <Col>
                        <div style={{ display: "flex" }}>
                        <Button style={{ marginLeft: "auto" }} color="primary" onClick={this.logOutUser}>Log out </Button>
                        </div>

                    </Col>
                </Row>

                <Card>
                    <br/>
                    <Row>
                        <Col>
                            Hello, {this.state.name}
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            this is your birthday: {this.state.birthdate}
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            this is your gender: {this.state.gender}
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            and this is your address: {this.state.address}
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col>
                            Below is your list of patients you're taking care of
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <PatientTableForCaregiver tableData = {this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>

                <React.Fragment>
                    <ul className="list-group">
                        {this.state.messages.map(message => (
                            <li className="list-group-item list-group-item-primary">
                                {message}
                            </li>
                        ))}
                    </ul>
                </React.Fragment>


            </div>
        )

    }
}


export default PatientsForCaregiverContainer;
