import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';

import Form from 'react-bootstrap/Form'
import PatientForm from "./components/patient-form";
import PatientFormEdit from "./components/patient-form-edit";

import * as API_USERS from "./api/patient-api"
import PatientTable from "./components/patient-table";
import NavigationBar from "../navigation-bar";



class PatientContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.toggleFormEdit = this.toggleFormEdit.bind(this);
        this.reload = this.reload.bind(this);
        this.reload_edit = this.reload_edit.bind(this);
        this.handleChangeInputDelete = this.handleChangeInputDelete.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.state = {
            selected: false,
            selected_edit: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            patientToBeDeleted: ""
        };
    }

    componentDidMount() {
        this.fetchPatients();
    }

    fetchPatients() {
        return API_USERS.getPatients((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    toggleFormEdit() {
        this.setState({selected_edit: !this.state.selected_edit});
    }


    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchPatients();
    }

    reload_edit() {
        this.setState({
            isLoaded: false
        });
        this.toggleFormEdit();
        this.fetchPatients();
    }

    deletePatient(patient) {
        return API_USERS.deletePatient(patient, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully deleted patient");
            } else {
                console.log("Could not delete patient");
            }

        });
    }

    handleDelete() {
        let patient = {
            name: this.state.patientToBeDeleted
        };

        console.log(patient);
        this.deletePatient(patient);
        this.setState({
            isLoaded: false
        });
        this.fetchPatients();
    }

    handleChangeInputDelete(event) {
        this.setState({patientToBeDeleted: event.target.value});
    }

    logOutUser() {
        return API_USERS.logOutUser( (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully logged out");
                window.location.href="/user/login"
            } else {
                console.log("Could not log out");
            }

        });
    }





    render() {
        return (
            <div>

                <NavigationBar />

                <CardHeader>
                    <strong> Patient Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col style={{padding: '5px'}} sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Patient </Button>
                        </Col>

                        <Col>
                            <div style={{ display: "flex" }}>
                                <Button style={{ marginLeft: "auto" }} color="primary" onClick={this.logOutUser}>Log out </Button>
                            </div>
                        </Col>

                        <Col style={{padding: '5px'}} sm={{size: '8', offset: 1 }}>
                            <Button color="primary" onClick={this.toggleFormEdit}>Edit Patient </Button>
                        </Col>
                        <Col sm={{size: '8', offset: 1}}>
                            <Form>
                                <Form.Group controlId="formBasicEmail">
                                    <Form.Label>Name of patient to delete</Form.Label>
                                    <Form.Control name="deleteInput" type="text" placeholder="Enter name. For example: John" value={this.state.patientToBeDeleted} onChange={this.handleChangeInputDelete}/>
                                </Form.Group>
                                <Button color="primary" onClick={this.handleDelete}>Delete Patient </Button>
                            </Form>
                        </Col>

                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <PatientTable tableData = {this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Patient: </ModalHeader>
                    <ModalBody>
                        <PatientForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selected_edit} toggle={this.toggleFormEdit}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormEdit}> Edit Patient: </ModalHeader>
                    <ModalBody>
                        <PatientFormEdit reloadHandler={this.reload_edit}/>
                    </ModalBody>
                </Modal>

            </div>
        )

    }
}


export default PatientContainer;
