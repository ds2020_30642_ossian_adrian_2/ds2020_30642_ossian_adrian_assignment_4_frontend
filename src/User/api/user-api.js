import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    user: '/user/login'
};


function logInUser(username, password, callback){
    let request = new Request(HOST.backend_api + endpoint.user + "/" + username + "/" + password, {
        method: 'GET',
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getCurrent(username, password, callback){
    let request = new Request(HOST.backend_api + "/user/current", {
        method: 'GET',
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export {
    logInUser,
    getCurrent
};
