import React from 'react';

import BackgroundImg from '../commons/images/wallpaper.jpg';

import {Button, Col, Container, Jumbotron, Row} from 'reactstrap';
import NavigationBar from "../navigation-bar";
import * as API_USERS from "../Patient/api/patient-api";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'white', };

class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: "",
            gender: "",
            address: "",
            isLoaded_user: false
        };
    }

    logOutUser() {
        return API_USERS.logOutUser( (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully logged out");
                window.location.href="/user/login"
            } else {
                console.log("Could not log out");
            }

        });
    }

    componentDidMount() {
        this.fetchUser();
    }

    fetchUser(){
        return API_USERS.getCurrent((result, status, err) => {
            if (result !== null && status === 200) {
                if(result.type === "doctor"){
                    this.setState({
                        name: result.name,
                        gender: result.gender,
                        address: result.adress,
                        isLoaded_user: true
                    });
                }
                else{
                    window.location.href="/user/login"
                }
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }



    render() {

        return (



            <div>
                <NavigationBar/>

                <Row>
                    {this.state.error_log_in > 0 && <Col>
                        Sorry, you can't access this page.
                    </Col>}
                </Row>

                <Row>
                    <Col>
                        <div style={{ display: "flex" }}>
                            <Button style={{ marginLeft: "auto" }} color="primary" onClick={this.logOutUser}>Log out </Button>
                        </div>

                    </Col>
                </Row>

                <Jumbotron fluid style={backgroundStyle}>
                    <Container fluid>
                        <h1 className="display-3" style={textStyle}>Integrated Medical Monitoring Platform for Home-care assistance</h1>
                        <p className="lead" style={textStyle}> <b>Enabling real time monitoring of patients, remote-assisted care services and
                            smart intake mechanism for prescribed medication.</b> </p>
                        <hr className="my-2"/>
                        <p  style={textStyle}> <b>Hello doctor, {this.state.name}. Your gender is {this.state.gender} and your address is {this.state.address}
                                                </b> </p>
                    </Container>
                </Jumbotron>

            </div>
        )
    };
}

export default Home
