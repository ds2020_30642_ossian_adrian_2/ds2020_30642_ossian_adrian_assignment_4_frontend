import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Home from './home/home';
import PersonContainer from './person/person-container'
import PatientContainer from './Patient/patient-container'
import CaregiverContainer from './Caregiver/caregiver-container'
import MedicationContainer from './Medication/medication-container'
import PatientsForCaregiverContainer from './Patient/caregiver-with-patients-container'
import PatientUIContainer from './Patient/patient-ui'
import MedicationTaken from './Patient/medication-taken'
import FormPage from './login/login-form'


import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';



class App extends React.Component {


    render() {

        return (
            <div className={styles.back}>
            <Router>
                <div>


                        <Switch>

                            <Route
                                exact
                                path='/doctor/home'
                                render={() => <Home/>}
                            />

                            <Route
                                exact
                                path='/doctor/person'
                                render={() => <PersonContainer/>}
                            />


                            <Route
                                exact
                                path='/doctor/patient'
                                render={() => <PatientContainer/>}
                            />

                            <Route
                                exact
                                path='/doctor/caregiver'
                                render={() => <CaregiverContainer/>}
                            />


                            <Route
                                exact
                                path='/patient/caregiver'
                                render={() => <PatientsForCaregiverContainer/>}
                            />


                            <Route
                                exact
                                path='/patient/medication'
                                render={() => <PatientUIContainer/>}
                            />

                            <Route
                                exact
                                path='/medicationtaken'
                                render={() => <MedicationTaken/>}
                            />


                            <Route
                                exact
                                path='/doctor/medication'
                                render={() => <MedicationContainer/>}
                            />

                            <Route
                                exact
                                path=''
                                render={() => <FormPage/>}
                            />

                            <Route
                                exact
                                path='/user/login'
                                render={() => <FormPage/>}
                            />


                            {/*Error*/}
                            <Route
                                exact
                                path='/error'
                                render={() => <ErrorPage/>}
                            />

                            <Route render={() =><ErrorPage/>} />
                        </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
