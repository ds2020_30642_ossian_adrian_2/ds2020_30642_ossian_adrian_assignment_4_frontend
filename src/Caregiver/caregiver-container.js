import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';

import Form from 'react-bootstrap/Form'
import CaregiverForm from "./components/caregiver-form";
import CaregiverFormEdit from "./components/caregiver-form-edit";

import * as API_USERS from "./api/caregiver-api"
import CaregiverTable from "./components/caregiver-table";
import NavigationBar from "../navigation-bar";
import * as API_LOGOUT from "../Patient/api/patient-api"



class CaregiverContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.toggleFormEdit = this.toggleFormEdit.bind(this);
        this.reload = this.reload.bind(this);
        this.reload_edit = this.reload_edit.bind(this);
        this.handleChangeInputDelete = this.handleChangeInputDelete.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.state = {
            selected: false,
            selected_edit: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            caregiverToBeDeleted: ""
        };
    }

    componentDidMount() {
        this.fetchCaregivers();
    }

    fetchCaregivers() {
        return API_USERS.getCaregivers((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    toggleFormEdit() {
        this.setState({selected_edit: !this.state.selected_edit});
    }


    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchCaregivers();
    }

    reload_edit() {
        this.setState({
            isLoaded: false
        });
        this.toggleFormEdit();
        this.fetchCaregivers();
    }

    deleteCaregiver(caregiver) {
        return API_USERS.deleteCaregiver(caregiver, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully deleted caregiver");
            } else {
                console.log("Could not delete caregiver");
            }

        });
    }

    handleDelete() {
        let caregiver = {
            name: this.state.caregiverToBeDeleted
        };

        console.log(caregiver);
        this.deleteCaregiver(caregiver);
        this.setState({
            isLoaded: false
        });
        this.fetchCaregivers();
    }

    handleChangeInputDelete(event) {
        this.setState({caregiverToBeDeleted: event.target.value});
    }

    logOutUser() {
        return API_LOGOUT.logOutUser( (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully logged out");
                window.location.href="/user/login"
            } else {
                console.log("Could not log out");
            }

        });
    }




    render() {
        return (
            <div>

                <NavigationBar />

                <CardHeader>
                    <strong> Caregiver Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col style={{padding: '5px'}} sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Caregiver </Button>
                        </Col>
                        <Col>
                            <div style={{ display: "flex" }}>
                                <Button style={{ marginLeft: "auto" }} color="primary" onClick={this.logOutUser}>Log out </Button>
                            </div>
                        </Col>


                        <Col style={{padding: '5px'}} sm={{size: '8', offset: 1 }}>
                            <Button color="primary" onClick={this.toggleFormEdit}>Edit Caregiver </Button>
                        </Col>
                        <Col sm={{size: '8', offset: 1}}>
                            <Form>
                                <Form.Group controlId="formBasicEmail">
                                    <Form.Label>Name of caregiver to delete</Form.Label>
                                    <Form.Control name="deleteInput" type="text" placeholder="Enter name. For example: John" value={this.state.caregiverToBeDeleted} onChange={this.handleChangeInputDelete}/>
                                </Form.Group>
                                <Button color="primary" onClick={this.handleDelete}>Delete Caregiver </Button>
                            </Form>
                        </Col>

                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <CaregiverTable tableData = {this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Caregiver: </ModalHeader>
                    <ModalBody>
                        <CaregiverForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selected_edit} toggle={this.toggleFormEdit}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormEdit}> Edit Caregiver: </ModalHeader>
                    <ModalBody>
                        <CaregiverFormEdit reloadHandler={this.reload_edit}/>
                    </ModalBody>
                </Modal>

            </div>
        )

    }
}


export default CaregiverContainer;
