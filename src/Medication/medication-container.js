import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';

import Form from 'react-bootstrap/Form'
import MedicationForm from "./components/medication-form";
import MedicationFormEdit from "./components/medication-form-edit";
import MedicalRecordForm from "./components/medication-form-medicalrecord";

import * as API_USERS from "./api/medication-api"
import MedicationTable from "./components/medication-table";
import NavigationBar from "../navigation-bar";
import * as API_LOGOUT from "../Patient/api/patient-api";



class MedicationContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.toggleFormEdit = this.toggleFormEdit.bind(this);
        this.toggleFormMedicalRecord = this.toggleFormMedicalRecord.bind(this);
        this.reload = this.reload.bind(this);
        this.reload_edit = this.reload_edit.bind(this);
        this.handleChangeInputDelete = this.handleChangeInputDelete.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.state = {
            selected: false,
            selected_edit: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            medicationToBeDeleted: "",
            selected_medicalrecord: false
        };
    }

    componentDidMount() {
        this.fetchMedications();
    }

    fetchMedications() {
        return API_USERS.getMedications((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    toggleFormEdit() {
        this.setState({selected_edit: !this.state.selected_edit});
    }

    toggleFormMedicalRecord() {
        this.setState({selected_medicalrecord: !this.state.selected_medicalrecord});
    }


    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchMedications();
    }

    reload_edit() {
        this.setState({
            isLoaded: false
        });
        this.toggleFormEdit();
        this.fetchMedications();
    }

    deleteMedication(medication) {
        return API_USERS.deleteMedication(medication, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully deleted medication");
            } else {
                console.log("Could not delete medication");
            }

        });
    }

    handleDelete() {
        let medication = {
            name: this.state.medicationToBeDeleted
        };

        console.log(medication);
        this.deleteMedication(medication);
        this.setState({
            isLoaded: false
        });
        this.fetchMedications();
    }

    handleChangeInputDelete(event) {
        this.setState({medicationToBeDeleted: event.target.value});
    }
    logOutUser() {
        return API_LOGOUT.logOutUser( (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully logged out");
                window.location.href="/user/login"
            } else {
                console.log("Could not log out");
            }

        });
    }




    render() {
        return (
            <div>

                <NavigationBar />

                <CardHeader>
                    <strong> Medication Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col style={{padding: '5px'}} sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Medication </Button>
                        </Col>
                        <Col>
                            <div style={{ display: "flex" }}>
                                <Button style={{ marginLeft: "auto" }} color="primary" onClick={this.logOutUser}>Log out </Button>
                            </div>
                        </Col>
                        <Col style={{padding: '5px'}} sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleFormMedicalRecord}>Create medical record </Button>
                        </Col>
                        <Col style={{padding: '5px'}} sm={{size: '8', offset: 1 }}>
                            <Button color="primary" onClick={this.toggleFormEdit}>Edit Medication </Button>
                        </Col>
                        <Col sm={{size: '8', offset: 1}}>
                            <Form>
                                <Form.Group controlId="formBasicEmail">
                                    <Form.Label>Name of medication to delete</Form.Label>
                                    <Form.Control name="deleteInput" type="text" placeholder="Enter name. For example: paracetamol" value={this.state.medicationToBeDeleted} onChange={this.handleChangeInputDelete}/>
                                </Form.Group>
                                <Button color="primary" onClick={this.handleDelete}>Delete Medication </Button>
                            </Form>
                        </Col>

                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <MedicationTable tableData = {this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Medication: </ModalHeader>
                    <ModalBody>
                        <MedicationForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selected_edit} toggle={this.toggleFormEdit}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormEdit}> Edit Medication: </ModalHeader>
                    <ModalBody>
                        <MedicationFormEdit reloadHandler={this.reload_edit}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selected_medicalrecord} toggle={this.toggleFormMedicalRecord}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormMedicalRecord}> Create medical record for patient: </ModalHeader>
                    <ModalBody>
                        <MedicalRecordForm />
                    </ModalBody>
                </Modal>

            </div>
        )

    }
}


export default MedicationContainer;
