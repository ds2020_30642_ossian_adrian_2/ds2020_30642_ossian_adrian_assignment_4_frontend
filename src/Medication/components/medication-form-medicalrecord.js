import React from 'react';
import validate from "./validators/medication-validators";
import Button from "react-bootstrap/Button";
import * as API_USERS from "../api/medication-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Col, Row} from "reactstrap";
import { FormGroup, Input, Label} from 'reactstrap';



class MedicalRecordForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                start_date: {
                    value: '',
                    placeholder: 'Provide the starting date in format YYYY-MM-DD',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                end_date: {
                    value: '',
                    placeholder: 'Provide the ending date in format YYYY-MM-DD',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                patientName: {
                    value: '',
                    placeholder: 'Provide a valid name of a patient (for example: John)',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                medicalCondition: {
                    value: '',
                    placeholder: 'Provide medical condition of the patient',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                intake_interval: {
                    value: '',
                    placeholder: 'Provide intake interval (for example: 8)',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },
                medicationsInOneString: {
                    value: '',
                    placeholder: 'Provide medication plan in the following format: medication1,medication2,medication3',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                }
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    registerMedicalRecord(medicalRecord) {
        return API_USERS.insertMedicalRecord(medicalRecord, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted medical record" );
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    handleSubmit() {
        let medicalrecord = {
            start_date: this.state.formControls.start_date.value,
            end_date: this.state.formControls.end_date.value,
            patientName: this.state.formControls.patientName.value,
            medicalCondition: this.state.formControls.medicalCondition.value,
            intake_interval: this.state.formControls.intake_interval.value,
            medicationsInOneString: this.state.formControls.medicationsInOneString.value
        };

        console.log(medicalrecord);
        this.registerMedicalRecord(medicalrecord);
    }

    render() {
        return (
            <div>

                <FormGroup id='start_date'>
                    <Label for='start_dateField'> Start date: </Label>
                    <Input name='start_date' id='start_dateField' placeholder={this.state.formControls.start_date.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.start_date.value}
                           touched={this.state.formControls.start_date.touched? 1 : 0}
                           valid={this.state.formControls.start_date.valid}
                           required
                    />
                    {this.state.formControls.start_date.touched && !this.state.formControls.start_date.valid &&
                    <div className={"error-message row"}> *  </div>}
                </FormGroup>

                <FormGroup id='end_date'>
                    <Label for='end_dateField'> End date: </Label>
                    <Input name='end_date' id='end_dateField' placeholder={this.state.formControls.end_date.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.end_date.value}
                           touched={this.state.formControls.end_date.touched? 1 : 0}
                           valid={this.state.formControls.end_date.valid}
                           required
                    />
                    {this.state.formControls.end_date.touched && !this.state.formControls.end_date.valid &&
                    <div className={"error-message"}> * </div>}
                </FormGroup>

                <FormGroup id='patientName'>
                    <Label for='patientNameField'> Patient name: </Label>
                    <Input name='patientName' id='patientNameField' placeholder={this.state.formControls.patientName.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.patientName.value}
                           touched={this.state.formControls.patientName.touched? 1 : 0}
                           valid={this.state.formControls.patientName.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='medicalCondition'>
                    <Label for='medicalConditionField'> Medical condition: </Label>
                    <Input name='medicalCondition' id='medicalConditionField' placeholder={this.state.formControls.medicalCondition.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.medicalCondition.value}
                           touched={this.state.formControls.medicalCondition.touched? 1 : 0}
                           valid={this.state.formControls.medicalCondition.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='intake_interval'>
                    <Label for='intake_intervalField'> Intake interval: </Label>
                    <Input name='intake_interval' id='intake_intervalField' placeholder={this.state.formControls.intake_interval.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.intake_interval.value}
                           touched={this.state.formControls.intake_interval.touched? 1 : 0}
                           valid={this.state.formControls.intake_interval.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='medicationsInOneString'>
                    <Label for='medicationsInOneStringField'> Medication: </Label>
                    <Input name='medicationsInOneString' id='medicationsInOneStringField' placeholder={this.state.formControls.medicationsInOneString.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.medicationsInOneString.value}
                           touched={this.state.formControls.medicationsInOneString.touched? 1 : 0}
                           valid={this.state.formControls.medicationsInOneString.valid}
                           required
                    />
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Submit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }
}

export default MedicalRecordForm;
